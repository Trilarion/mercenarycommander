/*
 * Copyright (c) 2010, Johan T. Larsson All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Mercenary Commander project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *      
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL JOHAN T. LARSSON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package games.TBSGame;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class StoryBriefing extends Activity implements OnClickListener  {
	private String StartGameButtonTag = "StartMission";
	
	private int CurrentLevel = 0;
	
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.storybriefing);
        TextView textview = (TextView)findViewById(R.id.BriefingTextView);
        textview.setText("This is the briefing for the mission.\n" +
        		"\n" +
        		"Multiple lines can be used.\n" +
        		"And also, this text is set by code, not in the .xml file.");
        
		Button startButton = (Button)findViewById(R.id.StartMissionButton);
		startButton.setOnClickListener(this);
    }

	@Override
	public void onClick(View button) {
		String tag = (String) button.getTag();
		if(tag.equals(StartGameButtonTag))
		{
			StartMission();
		}
	}
	
	public void StartMission()
	{
    	Bundle gameState = new Bundle();
    	gameState.putInt("GAME_STATE", Game.GAME_START);
    	gameState.putBoolean("GAME_MULTIPLAYER", false);
    	
    	switch (CurrentLevel) {
    	case 0:
    		gameState.putString("GAME_LEVEL_XML", "level_0.xml");
    		break;
    	}
    
    	Intent gameIntent = new Intent(this, Game.class);
    	gameIntent.putExtra("GAME_BUNDLE", gameState);
        startActivityForResult(gameIntent, 0);
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == RESULT_OK) {
			Bundle result = data.getBundleExtra("GAME_BUNDLE");
			int gameState = result.getInt("GAME_STATE"); 
			if(gameState == Game.GAME_OVER) {
				if(result.getInt("GAME_WINNER") == 0) {
					TextView textview = (TextView)findViewById(R.id.BriefingTextView);
			        textview.setText("You are the wiiiinner, my friieeeeeend...");
			        ++CurrentLevel;
				}
			}
			else if(gameState == Game.GAME_RUNNING) {
				Intent gameIntent = new Intent(this, Game.class);
		    	gameIntent.putExtra("GAME_BUNDLE", result);
		        startActivityForResult(gameIntent, 0);
			}
		}
	}
}
