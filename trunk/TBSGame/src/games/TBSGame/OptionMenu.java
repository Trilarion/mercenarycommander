package games.TBSGame;

import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;

public class OptionMenu extends PreferenceActivity {
	public void onCreate(Bundle instance)
	{
		super.onCreate(instance);

		setPreferenceScreen(CreatePreferences());
	}
	
	private PreferenceScreen CreatePreferences() {
        PreferenceScreen root = getPreferenceManager().createPreferenceScreen(getBaseContext());
        
        PreferenceCategory timingCat = new PreferenceCategory(this);
        timingCat.setTitle("Timing options");
        root.addPreference(timingCat);

        ListPreference listPref = new ListPreference(this);
        CharSequence[] tickEntries = new CharSequence[4];
        tickEntries[0] = "Instant";
        tickEntries[1] = "Fast";
        tickEntries[2] = "Medium";
        tickEntries[3] = "Slow";
        
        CharSequence[] tickValues = new CharSequence[4];
        tickValues[0] = "0";
        tickValues[1] = "100";
        tickValues[2] = "250";
        tickValues[3] = "500";
        
        listPref.setEntries(tickEntries);
        listPref.setEntryValues(tickValues);
        listPref.setDialogTitle("Turn Speed");
        listPref.setKey("PREF_TICK_DELAY");
        listPref.setTitle("End of Turn Speed");
        listPref.setSummary("Sets the speed with which the turn plays out after being finished.");
        timingCat.addPreference(listPref);
        
        return root;
    }
}
