/*
 * Copyright (c) 2010, Johan T. Larsson All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Mercenary Commander project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *      
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL JOHAN T. LARSSON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package games.TBSGame;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MultiPlayerMenu extends Activity implements OnClickListener {
	private String HostButtonTag = "HostMultiGameButton";
	private String ClientButtonTag = "ConnectMultiGameButton";
	private String RefreshButtonTag = "RefreshIpButton";
	
	public void onCreate(Bundle instance)
	{
		super.onCreate(instance);
		setContentView(R.layout.multiplayer);
		
		SetIpString();
		
		Button hostButton = (Button)findViewById(R.id.HostMultiGameButton);
		hostButton.setOnClickListener(this);
		
		Button clientButton = (Button)findViewById(R.id.ConnectMultiGameButton);
		clientButton.setOnClickListener(this);
		
		Button refreshButton = (Button)findViewById(R.id.RefreshIpButton);
		refreshButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View button) {
		String tag = (String) button.getTag();
		if(tag.equals(HostButtonTag))
		{
			int hostPort = GetHostPort();
			if(hostPort != 0)
			{
				HostGame(hostPort);
			}
		}
		else if(tag.equals(ClientButtonTag))
		{
		}
		else if(tag.equals(RefreshButtonTag))
		{
			SetIpString();
		}
	}
	
	private void HostGame(int port)
	{
		Bundle gameState = new Bundle();
    	gameState.putInt("GAME_STATE", Game.GAME_START);
    	gameState.putBoolean("GAME_MULTIPLAYER", true);
    	gameState.putInt("GAME_PORT", port);
    	gameState.putBoolean("GAME_HOST", true);
    	
    	switch (0) {
    	case 0:
    		gameState.putString("GAME_LEVEL_XML", "level_0.xml");
    		break;
    	}
    
    	Intent gameIntent = new Intent(this, Game.class);
    	gameIntent.putExtra("GAME_BUNDLE", gameState);
        startActivityForResult(gameIntent, 0);
	}
	
	private void SetIpString()
	{
		TextView ipText = (TextView)findViewById(R.id.IPOutput);
		String localIp = GetLocalIpAddress();
		if(localIp != null)
		{
			ipText.setText("Your IP is:\n" + localIp);
		}
		else
		{
			ipText.setText("No IP found.\nCheck your internet connection.");
		}
	}
	
	private int GetHostPort()
	{
		TextView portText = (TextView)findViewById(R.id.LocalPortInput);
		CharSequence port = portText.getText();
		if(port == null)
			return 0;
		return Integer.parseInt(port.toString());
	}
	
	private int GetRemotePort()
	{
		TextView portText = (TextView)findViewById(R.id.RemotePortInput);
		return Integer.parseInt(portText.getText().toString());
	}
	
	private String GetRemoteIP()
	{
		TextView portText = (TextView)findViewById(R.id.IPInput);
		return portText.getText().toString();
	}
	
	public String GetLocalIpAddress() {
	    try {
	        for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
	            NetworkInterface intf = en.nextElement();
	            for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
	                InetAddress inetAddress = enumIpAddr.nextElement();
	                if (!inetAddress.isLoopbackAddress()) {
	                    return inetAddress.getHostAddress().toString();
	                }
	            }
	        }
	    } catch (SocketException ex) {
	    	return null;
	    }
	    return null;
	}
}
