/*
 * Copyright (c) 2010, Johan T. Larsson All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Mercenary Commander project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *      
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL JOHAN T. LARSSON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package games.TBSGame;

import java.util.ArrayList;
import java.util.Iterator;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

public class OpponentAI extends Thread implements GameEnder, Opponent {
	
	Handler MainHandler;
	Handler LocalHandler;
	BattleWorld World;
	int LocalTeam = 1;
	
	public Handler Initialize(Handler mainHandler, BattleWorld world)
	{
		MainHandler = mainHandler;
		LocalHandler = new Handler() {
        	@Override
            public void handleMessage(Message msg) {
        		HandleMessage(msg);
            }
        };
		World = new BattleWorld(this, 1, LocalTeam);
		Bundle state = new Bundle();
		world.SaveState(state);
		World.RestoreState(state);
		return LocalHandler;
	}
	
    private void HandleMessage(Message msg)
    {
    	Bundle msgBundle = new Bundle();
    	Message reply = new Message();
    	reply.what = MSG_VOID;
    	
    	switch(msg.what)
    	{
    	case MSG_START_GAME:
    		CalculateTurn(msgBundle);
    		reply.what = MSG_RESULT_DONE;
    		break;
    	case MSG_HURRY_UP:
    		break;
    	case MSG_RESULT_DONE:
    		break;
    	case MSG_START_TURN:
    		World.Units.clear();
    		World.LoadUnitState(msg.getData(), World.Units);
    		CalculateTurn(msgBundle);
    		reply.what = MSG_RESULT_DONE;
    		break;
    	}
    	
    	if(reply.what != MSG_VOID)
    	{
    		reply.setData(msgBundle);
    		MainHandler.sendMessage(reply);
    	}
    }
    
    private void CalculateTurn(Bundle result)
    {
    	ArrayList<Unit> localUnits = new ArrayList<Unit>();
    	ArrayList<Unit> allUnits = World.Units;
    	
    	Iterator<Unit> it = allUnits.iterator();
    	Unit unit;
    	while(it.hasNext())
    	{
    		unit = it.next();
    		if(unit.Team == LocalTeam)
    		{
    			localUnits.add(unit);
    		}
    	}
    	
    	it = localUnits.iterator();
    	while(it.hasNext())
    	{
    		unit = it.next();
    		unit.OrderTo(unit.PosX + 1, unit.PosY);
    	}
    	World.PackageUnits(result, localUnits);
    }

	@Override
	public void EndGame(int winner, int optionalGoals) {
		// Empty
	}
}
