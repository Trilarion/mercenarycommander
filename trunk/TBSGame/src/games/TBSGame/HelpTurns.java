/*
 * Copyright (c) 2010, Johan T. Larsson All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Mercenary Commander project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *      
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL JOHAN T. LARSSON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package games.TBSGame;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ScrollView;
import android.widget.TextView;

public class HelpTurns extends Activity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ScrollView mainView = new ScrollView(this);
        
        TextView textView = new TextView(this);
        textView.setText("The game is based on simultaneous turns. Each turn has two phases: the interactive phase and the execution phase.\n" +
        		"\n" +
        		"==Interactive Phase==\n" +
        		"During the interactive phase, the player may give his units orders, scan the map, plan his strategies, etc. Nothing done by the player has any immediate effect on the game world.\n" +
        		"To zoom in and out on the battlefield, the player uses a pinch gesture or the zoom controls at the bottom of the screen. To scroll the battlefield, he touches the screen with one finger and immediately moves it in any direction. The map will follow the finger of the player.\n" +
        		"To check the details of a particular unit or hex, the player briefly taps the screen at the desired unit or hex, then presses the menu button and chooses \"selection info\".\n" +
        		"Once the player is happy with the orders he has given his units, he should press the End Turn button. Once all players have done so, the interactive phase ends and the execution phase begins.\n" +
        		"\n" +
        		"==Execution Phase==\n" +
        		"During the execution phase, each unit performs its given orders. The execution phase is divided into 12 \"ticks\", and during each tick each unit performs at most one action. So, if a unit has a movement speed of 3 hexes/turn, it would move once every 12/3=4 ticks");
        
        mainView.addView(textView);
        setContentView(mainView);
    }
}
