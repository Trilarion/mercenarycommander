/*
 * Copyright (c) 2010, Johan T. Larsson All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Mercenary Commander project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *      
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL JOHAN T. LARSSON BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

package games.TBSGame;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainMenu extends Activity implements OnClickListener {
	private String SinglePlayerButtonTag = "SPButton";
	private String MultiPlayerButtonTag = "MPButton";
	private String OptionButtonTag = "OptionButton";
	private String CreditButtonTag = "CreditButton";
	private String HelpButtonTag = "HelpButton";
	
	public void onCreate(Bundle instance)
	{
		super.onCreate(instance);
		setContentView(R.layout.mainmenu);
		findViewById(R.id.SPButton).setOnClickListener(this);
		findViewById(R.id.MPButton).setOnClickListener(this);
		findViewById(R.id.OptionButton).setOnClickListener(this);
		findViewById(R.id.CreditButton).setOnClickListener(this);
		findViewById(R.id.HelpButton).setOnClickListener(this);
	}

	@Override
	public void onClick(View button) {
		String tag = (String) button.getTag();
		if(tag.equals(SinglePlayerButtonTag))
		{
			StartSinglePlayer();
		}
		else if(tag.equals(MultiPlayerButtonTag))
		{
			StartMultiPlayer();
		}
		else if(tag.equals(OptionButtonTag))
		{
			StartOptions();
		}
		else if(tag.equals(CreditButtonTag))
		{
			StartCredits();
		}
		else if(tag.equals(HelpButtonTag))
		{
			StartHelp();
		}
	}
	
	public void StartSinglePlayer()
	{
    	Intent gameIntent = new Intent(this, SinglePlayerMenu.class);
        startActivity(gameIntent);
	}
	
	public void StartMultiPlayer()
	{
    	Intent gameIntent = new Intent(this, MultiPlayerMenu.class);
        startActivity(gameIntent);
	}
	
	public void StartOptions()
	{
    	Intent optionIntent = new Intent(this, OptionMenu.class);
        startActivity(optionIntent);
	}
	
	public void StartHelp()
	{
    	Intent helpIntent = new Intent(this, HelpHolder.class);
        startActivity(helpIntent);
	}
	
	public void StartCredits()
	{
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
	    builder.setCancelable(true);
	    builder.setTitle("Credits, where due:");
		builder.setMessage("Johan Larsson:\n" +
				"  Lead programmer\n" +
				"  Horrible graphics");
	    builder.setInverseBackgroundForced(true);
	    builder.setNeutralButton("Done", new DialogInterface.OnClickListener() {
	      @Override
	      public void onClick(DialogInterface dialog, int which) {
	        dialog.dismiss();
	      }
	    });

	    AlertDialog alert = builder.create();
	    alert.show();
	}
}
